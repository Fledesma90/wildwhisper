using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1Script : MonoBehaviour
{
    //reference to the player gameobject
    [SerializeField] GameObject playerGO;
    //reference to the player movement script
    [SerializeField] PlayerMovement playerMovementScript;
    //bool to confirm the enemy can attack
    [SerializeField] bool canAttack;
    //bool to check if the enemy is dead or not
    [SerializeField] bool dead;
    //time to wait for the enemy to attack
    [SerializeField] float timeToAttack;
    //time to change between attack mode or idle vulnerable state
    [SerializeField] float decisionTime=3;
    //collider reference
    private BoxCollider2D col;
    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<BoxCollider2D>();
        playerGO = GameObject.FindGameObjectWithTag("Player");
        playerMovementScript = playerGO.GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update() {
        CounterToActivateCollider();
        ColliderActivation();

    }

    /// <summary>
    /// If the time to attack is greater than decision time then the enemy attacks
    /// by activating the bool to attack
    /// </summary>
    private void CounterToActivateCollider() {
        if (canAttack == false) {
            timeToAttack += Time.deltaTime;
        }
        if (decisionTime <= timeToAttack) {
            canAttack = true;
        }
        if (canAttack == true) {
            timeToAttack -= Time.deltaTime;
        }
        if (timeToAttack <= 0) {

            canAttack = false;
        }
    }
    /// <summary>
    /// the bool to attack determines the activation of the collider's trigger
    /// </summary>
    void ColliderActivation() {
        if (canAttack==true) {
            col.isTrigger = true;
        }
        if (canAttack==false) {
            col.isTrigger = false;
                
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<PlayerMovement>().gameObject) {
            Debug.Log("te ataco");
            playerGO.GetComponent<Rigidbody2D>().AddForce(transform.right * 1000);
            StartCoroutine(PlayerAttackedAction());
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.GetComponent<PlayerMovement>()) {
            col.enabled = false;
            Debug.Log("me matas");
            dead = true;
            //TODO animación muerte
            //TODO sumar almas al personake
            //TODO sonidos
            //TODO añadir algún efecto visual por ejemplo añadir una imagen de daño
            Destroy(gameObject, 2);
            
        }
    }

    IEnumerator PlayerAttackedAction() {
        playerMovementScript.touchedEnemy = true;
        //animación toreo
        yield return new WaitForSeconds(5);
        //animacion idle
        playerMovementScript.touchedEnemy = false;
    }
}
