using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerSoulScore : MonoBehaviour
{
    public int soulScore;
    public int displayedScore;
    public float velocityToRiseScore;
    public TextMeshProUGUI soulScoreText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        //UpdateSoulScore();

    }
    /// <summary>
    /// sumamos los puntos de manera progresiva
    /// </summary>
    //private void UpdateSoulScore() {
    //    if (soulScore>=displayedScore) {
    //        displayedScore += Time.deltaTime*velocityToRiseScore;
    //        soulScoreText.text = displayedScore.ToString("F0");
    //    } else {
    //        return;
    //    }
        
        
    //}

    /// <summary>
    /// añadimos puntuación tras recoger almas
    /// </summary>
    /// <param name="scoreToAdd"></param>
    public void AddSoulScore(int soulScoreToAdd) {
        
        soulScore += soulScoreToAdd;
        StartCoroutine(AddScore(soulScoreToAdd));
    }

    IEnumerator AddScore( int soulScoreToAdd) {

        for (int i = 0; i < soulScoreToAdd; i++) {
            displayedScore++;
            soulScoreText.text = (displayedScore).ToString();
            yield return new WaitForSeconds(velocityToRiseScore);
        }
    }

}
