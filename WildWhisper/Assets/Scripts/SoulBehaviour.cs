using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulBehaviour : MonoBehaviour
{
    public int powerUpScore;
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<PlayerMovement>().touchedEnemy==false) {
            collision.GetComponent<PlayerSoulScore>().AddSoulScore(powerUpScore);
            Destroy(gameObject);
        }
    }
}
