using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //es estática porque no vamos a usar otro gamemanager y lo ponemos statico porque será legítimo
    public static GameManager instance;

    public float scoreMeters;
    public float scoreSouls;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void SaveScore() {
        scoreMeters = Mathf.Round(PlayerMovement.instance.meters);
    }

    void GoMainMenu() {
        SceneManager.LoadScene(0);
    }

  
}
