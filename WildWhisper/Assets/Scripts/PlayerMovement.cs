using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    [Header("Jump Settings")]
    //force of jump of player
    public float jumpForce = 7f;
    public int jumpsInAirCurrent = 0;
    public int jumpsInAirAllowed = 1;
    [Header("Ground Check")]
    //object that is going to check if the player is in touch with the ground
    public Transform groundCheck;
    //layer considered as ground
    public LayerMask groundLayer;
    //Size of the area this gameobject is going to check if it is in touch with
    //the ground
    public Vector2 sizeGroundCheck = new Vector2(0.64f, 0.09f);
    //bool that is going to check if the player is touching the ground
    public bool grounded = false;
    //max speed allowed
    public float maxSpeed = 5f;
    //acceleration applied
    public float acceleration = 20f;
    //rigidbody reference
    private Rigidbody2D rb;
    //meters the player is reaching
    public float meters = 0f;
    //Start position of the player.
    private Vector2 startPosition;
    //TODO text that will inform the user the walked distance
    public TextMeshProUGUI metersText;
    //force of gravity when just pressing the 
    public float gravityNormal;
    //When tapping twice and hold the button your jump will increase or decrease
    public float gravityHolding;

    public static PlayerMovement instance;
    //bool to check if player has entered in contact with an enemy
    public bool touchedEnemy;
    [Header("Movement")]
    //bool to inform the player is going to be continuously moving
    public bool autoMove = true;
    // Start is called before the first frame update

    private void Awake() {
        if (instance==null) {
            instance = this;
        }
    }
    void Start()
    {
        //we pick up the start position to start counting the distance
        startPosition = transform.position;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        EvaluatePhysics();
        CountDistance();
    }

    private void LateUpdate() {
        Movement();
    }
    /// <summary>
    /// Draw gizmos in red to visualize where is the ground
    /// </summary>
    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(groundCheck.position, sizeGroundCheck);
    }
    /// <summary>
    /// Check physics with the ground
    /// </summary>
    void EvaluatePhysics() {
        grounded = Physics2D.OverlapBox(groundCheck.position, sizeGroundCheck, 0f, groundLayer);
        //if the player is touching the ground the current jumps are 0
        if (grounded) {
            jumpsInAirCurrent = 0;
            
            //TODO jump animation is false
        } else {
            //if player is on the air current jumps are 1
            jumpsInAirCurrent = 1;
            //TODO jump animation is true
        }
        
        if (rb.velocity.y>0) {
            //TODO animation of going up
        }

        if (rb.velocity.y<0) {
            //TODO animation of going down
        }
        
    }
    /// <summary>
    /// Controls interaction
    /// </summary>
    void Controls() {
        
    }
    /// <summary>
    /// Jump action
    /// </summary>
    public void Jump() {
        if (grounded||jumpsInAirCurrent<jumpsInAirAllowed) {
            rb.velocity = new Vector2(rb.velocity.x, 0f);
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            
        }
    }
    /// <summary>
    /// Stablish automovement
    /// </summary>
    public void Movement() {
        //if automove is not activated or gameobject is not touching the ground then not move
        if (!autoMove || (!grounded && rb.velocity.y<0)) {
            return;
        }
        //store current speed of rigidbody
        Vector2 tempVelocity = rb.velocity;
        //x value (horizontal) is modified to be approximated to the desired speed, through a 
        //progressive acceleration.
        //X value is multiplied by timedeltatime so the corresponding acceleration is applied
        //according to the time fraction of happened since the last frame
        tempVelocity.x = Mathf.MoveTowards(tempVelocity.x, maxSpeed, Time.deltaTime * acceleration);

        rb.velocity = tempVelocity;
    }
    /// <summary>
    /// Stop or resume automovement
    /// </summary>
    public void StopMovement() {
        autoMove = !autoMove;
        rb.velocity = Vector3.zero;
    }
    /// <summary>
    /// Count distance and show in HUD the meters 
    /// </summary>
    private void CountDistance() {
        meters = Vector2.Distance(startPosition, transform.position);
        metersText.text = meters.ToString("F0") + "meters";
        GameManager.instance.SaveScore();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        //TODO behaviour when contacting with bullfighter
        //todo behaviour when contacting with a split on the wall
        //todo behaviour when finishing the level
        
    }

    
}
