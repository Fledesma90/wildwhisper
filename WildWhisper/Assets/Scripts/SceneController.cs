using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneController : MonoBehaviour
{

    #region ############ VARIABLES
    //current scene
    private int currentSceneIndex;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        Debug.Log(currentSceneIndex);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void ExitGame() {
        Application.Quit();
    }

    public void LoadLevel1() {
        if (currentSceneIndex==0) {
            SceneManager.LoadScene(1);
        }
    }
}
