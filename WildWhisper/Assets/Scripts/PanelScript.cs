using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PanelScript : MonoBehaviour
{
    public GameObject pauseMenu;
    public bool paused = false;
    public GameObject[] playerButtons;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    /// <summary>
    /// Cambia el estado del tiempo
    /// </summary>
    /// <returns></returns>
    public float TimeScale() {
        paused = !paused;

        if (paused) {
            foreach (GameObject button in playerButtons) {
                button.GetComponent<Button>().interactable = false;
            }
            return 0;
        } else {
            foreach (GameObject button in playerButtons) {
                button.GetComponent<Button>().interactable = true;
            }
            return 1f;
        }
    }
    /// <summary>
    /// Realiza las acciones necesarias para pausar o renaudar la partida
    /// </summary>
    public void Pause() {

        pauseMenu.SetActive(!pauseMenu.activeSelf);

        TimeState();


    }
    /// <summary>
    /// Establece la velocidad del tiempo
    /// </summary>
    public void TimeState() {
        Time.timeScale = TimeScale();
    }

}
